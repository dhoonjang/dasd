import React, { useState, useMemo } from "react";

export enum EPopUpType {
  createGroup,
  requestGroup,
  deleteGroup,
  reserveGH,
}

export type TSetPopUp = (popUp: EPopUpType | null) => void;

export interface IPopUpContext {
  popUp: EPopUpType | null;
  setPopUp: TSetPopUp;
}

export const PopUpContext = React.createContext<IPopUpContext>({
  popUp: null,
  setPopUp: (popUp: EPopUpType | null) => {},
});

const PopUpContextProvider: React.FC<{ children: React.ReactChild }> = ({
  children,
}) => {
  const [popUp, setPopUp] = useState<EPopUpType | null>(null);
  const popUpContext: IPopUpContext = useMemo(() => ({ popUp, setPopUp }), [
    popUp,
  ]);

  return (
    <PopUpContext.Provider value={popUpContext}>
      {children}
    </PopUpContext.Provider>
  );
};

export default PopUpContextProvider;
