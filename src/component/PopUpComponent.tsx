import React, { useContext } from "react";
// import { RouteUrlMove } from "../../tool/urlTool";
// import { useHistory } from "react-router-dom";
import { PopUpContext, EPopUpType } from "../context/PopUpContextProvider";
import ReservePopUp from "./ReservePopUp";

export interface IErrorComponentProps {
  pageName?: string;
}

const PopUpComponent: React.FC = () => {
  const { popUp, setPopUp } = useContext(PopUpContext);

  const closeFunc = () => {
    setPopUp(null);
  };

  if (popUp !== null && popUp !== EPopUpType.reserveGH) {
    return (
      <div className="submit PopUp" onClick={closeFunc}>
        <div className="content-box">
          {popUp === EPopUpType.createGroup && "그룹이 생성되었습니다!"}
          {popUp === EPopUpType.requestGroup && "그룹에 신청을 보냈습니다!"}
          {popUp === EPopUpType.deleteGroup && "그룹에서 탈퇴되었습니다!"}
        </div>
      </div>
    );
  } else if (popUp === EPopUpType.reserveGH) {
    return <ReservePopUp closeFunc={closeFunc} />;
  }

  return <></>;
};

export default PopUpComponent;
