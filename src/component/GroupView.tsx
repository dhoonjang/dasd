import React, { useContext } from "react";
import { IGroup, IMember } from "../api/apiDefaultRes";
import { useHistory } from "react-router-dom";
import { PopUpContext, EPopUpType } from "../context/PopUpContextProvider";

const GroupView: React.FC<{
  group: IGroup;
  member: IMember[];
  isMy?: boolean;
}> = ({ group, member, isMy }) => {
  const history = useHistory();
  const { setPopUp } = useContext(PopUpContext);
  const memberView = member.map((m) => (
    <div className="member">
      {m.isLeader && <div className="leader">Leader</div>}
      <div className="name">{m.name}</div>
      <div className="gender">{m.gender ? "여자" : "남자"}</div>
    </div>
  ));

  const requestFunc = () => {
    setPopUp(EPopUpType.requestGroup);
    history.push("/");
  };
  const deleteFunc = () => {
    setPopUp(EPopUpType.deleteGroup);
    history.push("/");
  };

  return (
    <div className="GroupView">
      <div className="name item">
        <div className="label">그룹 이름</div>
        {group.name}
      </div>
      <div className="destination item">
        <div className="label">여행 지역</div>
        {group.destination}
      </div>
      <div className="date item">
        <div className="label">여행 날짜</div>
        {group.date[0]} ~ {group.date[1]}
      </div>
      <div className="capacity item">
        <div className="label">남은 인원</div>
        {group.capacity - group.memberNum} / {group.capacity}
      </div>
      <div className="member-area item">
        <div className="label">멤버 리스트</div>
        {memberView}
      </div>
      {isMy ? (
        <button className="request-btn" onClick={deleteFunc}>
          그룹 탈퇴
        </button>
      ) : (
        <button className="request-btn" onClick={requestFunc}>
          그룹 신청
        </button>
      )}
    </div>
  );
};

export default GroupView;
