import React from "react";
import { IPlace } from "../api/apiDefaultRes";

const RecommendView: React.FC<{
  place: IPlace[];
  type?: string;
  introduce?: string;
}> = ({ place, type, introduce }) => {
  const placeView = place.map((p) => (
    <div className="place-view">
      <div className="name">{p.컨텐츠명}</div>
      <div className="category">{p.카테고리2}</div>
      <div className="introudce">{p.소개}</div>
      <div className="address">{p.신주소 ? p.신주소 : p.지번주소}</div>
      {p.홈페이지.length > 0 && (
        <a href={p.홈페이지} target="black">
          홈페이지 링크
        </a>
      )}
    </div>
  ));

  return (
    <div className="RecommendView">
      {type && (
        <div className="type">
          <h2>
            당신의 여행자 유형은? <span>{type}</span>
          </h2>
          {introduce}
        </div>
      )}
      <h2>추천 여행지</h2>
      {placeView}
    </div>
  );
};

export default RecommendView;
