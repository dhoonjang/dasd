import React, { useState, useReducer } from "react";
import {
  reserveFormReducer,
  initialReserveForm,
  EReserveFormActionType,
} from "../reducer/reserveFrom";

const ReservePopUp: React.FC<{ closeFunc: () => void }> = ({ closeFunc }) => {
  const [step, setStep] = useState<number>(0);
  const [reserveForm, dispatchReserveForm] = useReducer(
    reserveFormReducer,
    initialReserveForm
  );

  const submitFunc = () => {
    if (step === 0) {
      setStep(1);
      setTimeout(() => {
        setStep(2);
        setTimeout(() => {
          setStep(3);
        }, 2000);
      }, 2000);
    } else if (step === 3) {
      closeFunc();
    }
  };

  if (step !== 0) {
    return (
      <div className="PopUp">
        <div className="content-box" onClick={submitFunc}>
          {step === 1 && "결제 진행 중..."}
          {step === 2 && "예약 진행 중..."}
          {step === 3 && "예약이 완료되었습니다!"}
        </div>
      </div>
    );
  }
  return (
    <div className="PopUp">
      <div className="content-box">
        <h3 className="label">예약날짜</h3>
        <div className="date-flex">
          <input
            className="date-input"
            type="text"
            placeholder="예) 20200803"
            onChange={(e) =>
              dispatchReserveForm({
                type: EReserveFormActionType.setStartDate,
                str: e.currentTarget.value,
              })
            }
            value={reserveForm.date[0]}
          />
          <div>~</div>
          <input
            className="date-input"
            type="text"
            placeholder="예) 20200806"
            onChange={(e) =>
              dispatchReserveForm({
                type: EReserveFormActionType.setEndDate,
                str: e.currentTarget.value,
              })
            }
            value={reserveForm.date[1]}
          />
        </div>
        <h3 className="label">카드 번호</h3>
        <input
          className="pf-input"
          type="text"
          placeholder="예) 0000-0000-0000-0000"
          onChange={(e) =>
            dispatchReserveForm({
              type: EReserveFormActionType.setCardNum,
              str: e.currentTarget.value,
            })
          }
          value={reserveForm.cardNum}
        />
        <h3 className="label">CVC 번호</h3>
        <input
          className="pf-input"
          type="text"
          placeholder="예) 000"
          onChange={(e) =>
            dispatchReserveForm({
              type: EReserveFormActionType.setCVCNum,
              str: e.currentTarget.value,
            })
          }
          value={reserveForm.cvcNum}
        />
        <h3 className="label">카드 만료일</h3>
        <input
          className="pf-input"
          type="text"
          placeholder="예) 09/24"
          onChange={(e) =>
            dispatchReserveForm({
              type: EReserveFormActionType.setExpireDate,
              str: e.currentTarget.value,
            })
          }
          value={reserveForm.expireDate}
        />
        <div className="btn-flex">
          <button className="request-btn" onClick={submitFunc}>
            예약 진행
          </button>
          <button className="request-btn" onClick={closeFunc}>
            예약 취소
          </button>
        </div>
      </div>
    </div>
  );
};

export default ReservePopUp;
