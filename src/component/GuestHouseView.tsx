import React, { useContext } from "react";
import {
  IPlace,
  defaultCapacity,
  defaultDateArray,
} from "../api/apiDefaultRes";
import { PopUpContext, EPopUpType } from "../context/PopUpContextProvider";

const GuestHouseView: React.FC<{ guestHouse: IPlace }> = ({ guestHouse }) => {
  const { setPopUp } = useContext(PopUpContext);
  const dateArray = guestHouse.date ? guestHouse.date : defaultDateArray;
  const submitFunc = () => {
    setPopUp(EPopUpType.reserveGH);
  };
  return (
    <div className="GuestHouseView">
      <div className="name item">
        <div className="label">게스트하우스 이름</div>
        {guestHouse.컨텐츠명}
      </div>
      <div className="destination item">
        <div className="label">게스트하우스 주소</div>
        {guestHouse.신주소}
      </div>
      <div className="date item">
        <div className="label">예약 가능 날짜</div>
        {dateArray.map((d) => (
          <span>
            {d}
            <br />
          </span>
        ))}
      </div>
      <div className="capacity item">
        <div className="label">예약 가능 인원</div>
        여자:{" "}
        {guestHouse.woman_capacity
          ? guestHouse.woman_capacity
          : defaultCapacity[0]}
        <br />
        남자:{" "}
        {guestHouse.man_capacity ? guestHouse.man_capacity : defaultCapacity[1]}
      </div>
      <div className="capacity item">
        <div className="label">전화 번호</div>
        {guestHouse.전화번호}
      </div>
      <button className="request-btn" onClick={submitFunc}>
        게스트하우스 예약
      </button>
    </div>
  );
};

export default GuestHouseView;
