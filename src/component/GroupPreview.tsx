import React from "react";
import { IGroup } from "../api/apiDefaultRes";
import { Link } from "react-router-dom";

const GroupPreview: React.FC<{ group: IGroup; isMy?: boolean }> = ({
  group,
  isMy,
}) => {
  return (
    <Link
      to={`/${isMy ? "group" : "home"}/${group.id}`}
      className="GroupPreview"
    >
      <div className="name item">
        <div className="label">그룹 이름</div>
        {group.name}
      </div>
      <div className="destination item">
        <div className="label">여행 지역</div>
        {group.destination}
      </div>
      <div className="date">
        <div className="label">여행 날짜</div>
        {group.date[0]}
        <br />~<br />
        {group.date[1]}
      </div>
      <div className="capacity item">
        <div className="label">남은 인원</div>
        {group.capacity - group.memberNum} / {group.capacity}
      </div>
    </Link>
  );
};

export default GroupPreview;
