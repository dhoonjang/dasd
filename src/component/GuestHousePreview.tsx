import React from "react";
import {
  IPlace,
  defaultDateArray,
  defaultCapacity,
} from "../api/apiDefaultRes";
import { Link } from "react-router-dom";

const GuestHousePreview: React.FC<{ guestHouse: IPlace }> = ({
  guestHouse,
}) => {
  const dateArray = guestHouse.date ? guestHouse.date : defaultDateArray;

  return (
    <Link to={`/reserve/${guestHouse.일련번호}`} className="GuestHousePreview">
      <div className="name item">
        <div className="label">그룹 이름</div>
        {guestHouse.컨텐츠명}
      </div>
      <div className="destination item">
        <div className="label">지역 주소</div>
        {guestHouse.신주소}
      </div>
      <div className="date">
        <div className="label">예약 가능 날짜</div>
        {dateArray.map((d) => (
          <span>
            {d}
            <br />
          </span>
        ))}
      </div>
      <div className="capacity item">
        <div className="label">남은 인원</div>
        여자:{" "}
        {guestHouse.woman_capacity
          ? guestHouse.woman_capacity
          : defaultCapacity[0]}
        <br />
        남자:{" "}
        {guestHouse.man_capacity ? guestHouse.man_capacity : defaultCapacity[1]}
      </div>
    </Link>
  );
};

export default GuestHousePreview;
