import React, { useReducer, useState } from "react";
import { testApi, IRecommend } from "../api/apiDefaultRes";
import { testFormReducer, initialTestForm } from "../reducer/testForm";
import RecommendView from "./RecommendView";

const TestView: React.FC<{ recommend: IRecommend }> = ({ recommend }) => {
  const [isAnswer, setIsAnswer] = useState(false);
  const [testForm, dispatchTestForm] = useReducer(
    testFormReducer,
    initialTestForm
  );

  const questionView = testApi.list.map((q, i) => (
    <div className="question">
      <div className="txt">{q.question}</div>
      <div className="btn-area">
        <button
          className={`select-btn ${testForm.test[i] && "select"}`}
          onClick={() => dispatchTestForm({ index: i, bool: true })}
        >
          {q.trueAnswer}
        </button>
        <button
          className={`select-btn ${!testForm.test[i] && "select"}`}
          onClick={() => dispatchTestForm({ index: i, bool: false })}
        >
          {q.falseAnswer}
        </button>
      </div>
    </div>
  ));

  return (
    <div className="TestView">
      {!isAnswer ? (
        <div className="question-area">
          <h2>성향 테스트</h2>
          {questionView}
          <button className="request-btn" onClick={() => setIsAnswer(true)}>
            응답 완료
          </button>
        </div>
      ) : (
        <RecommendView
          place={recommend.testplace}
          type={recommend.user_type}
          introduce={recommend.introduce}
        />
      )}
    </div>
  );
};

export default TestView;
