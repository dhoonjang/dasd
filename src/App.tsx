import React from "react";
import { BrowserRouter, Route, Switch, Link, Redirect } from "react-router-dom";
import Post from "./pages/Post";
import Group from "./pages/Group";
import Reserve from "./pages/Reserve";
import Recommend from "./pages/Recomend";
import Home from "./pages/Home";
import "./App.scss";
import PopUpContextProvider from "./context/PopUpContextProvider";
import PopUpComponent from "./component/PopUpComponent";

function App() {
  return (
    <div className="App">
      <PopUpContextProvider>
        <BrowserRouter>
          <PopUpComponent />
          <div className="body">
            <Switch>
              <Route path="/home/:group_id" component={Home} />
              <Route path="/group/:group_id" component={Group} />
              <Route path="/reserve/:guesthouse_id" component={Reserve} />
              <Route path="/recommend" component={Recommend} />
              <Route path="/post" component={Post} />
              <Route path="/" render={() => <Redirect to="/home/default" />} />
            </Switch>
          </div>
          <div className="nav-flex">
            <Link className="nav" to="/home/default">
              HOME
            </Link>
            <Link className="nav" to="/group/default">
              GROUP
            </Link>
            <Link className="nav" to="/recommend">
              RECOMMEND
            </Link>
            <Link className="nav" to="/reserve/default">
              RESERVE
            </Link>
          </div>
        </BrowserRouter>
      </PopUpContextProvider>
    </div>
  );
}

export default App;
