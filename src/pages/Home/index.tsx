import React, { useMemo } from "react";
import "./Home.scss";
import { groupApi } from "../../api/apiDefaultRes";
import GroupPreview from "../../component/GroupPreview";
import { useParams, Link } from "react-router-dom";
import GroupView from "../../component/GroupView";

function Home() {
  const { group_id } = useParams();

  const groupListView = groupApi.list.map((g) => (
    <GroupPreview key={g.id} group={g} />
  ));

  const selectGroup = groupApi.list.find((g) => String(g.id) === group_id);

  const groupView = useMemo(() => {
    if (!selectGroup) return <></>;
    return <GroupView group={selectGroup} member={groupApi.selectMember} />;
  }, [selectGroup]);

  return (
    <div className="Home">
      <h1>그룹 리스트</h1>
      {selectGroup ? groupView : groupListView}
      <Link to="/post" className="post-group">
        NEW
        <br />
        GROUP
      </Link>
    </div>
  );
}

export default Home;
