import React, { useReducer, useContext } from "react";
import "./Post.scss";
import {
  postFormReducer,
  initialPostForm,
  EPostFormActionType,
} from "../../reducer/postForm";
import { PopUpContext, EPopUpType } from "../../context/PopUpContextProvider";
import { useHistory } from "react-router-dom";

function Post() {
  const history = useHistory();
  const { setPopUp } = useContext(PopUpContext);
  const [postForm, dispatchPostFrom] = useReducer(
    postFormReducer,
    initialPostForm
  );

  const submitFunc = () => {
    setPopUp(EPopUpType.createGroup);
    history.push("/");
  };
  return (
    <div className="Post">
      <h1>새로운 그룹 포스팅</h1>
      <div className="label">그룹 이름</div>
      <input
        className="pf-input"
        type="text"
        placeholder="예) 가자 서울로"
        onChange={(e) =>
          dispatchPostFrom({
            type: EPostFormActionType.setName,
            str: e.currentTarget.value,
          })
        }
        value={postForm.name}
      />
      <div className="label">여행 지역</div>
      <input
        className="pf-input"
        type="text"
        placeholder="예) 서울특별시"
        onChange={(e) =>
          dispatchPostFrom({
            type: EPostFormActionType.setDestination,
            str: e.currentTarget.value,
          })
        }
        value={postForm.destination}
      />
      <div className="label">여행날짜</div>
      <div className="date-flex">
        <input
          className="date-input"
          type="text"
          placeholder="예) 20200803"
          onChange={(e) =>
            dispatchPostFrom({
              type: EPostFormActionType.setStartDate,
              str: e.currentTarget.value,
            })
          }
          value={postForm.startDate}
        />
        <div>~</div>
        <input
          className="date-input"
          type="text"
          placeholder="예) 20200806"
          onChange={(e) =>
            dispatchPostFrom({
              type: EPostFormActionType.setEndDate,
              str: e.currentTarget.value,
            })
          }
          value={postForm.endDate}
        />
      </div>
      <div className="label">그룹인원</div>
      <div className="capacity-flex">
        <div
          className="minus-btn"
          onClick={() =>
            dispatchPostFrom({ type: EPostFormActionType.minusCapacity })
          }
        >
          -
        </div>
        <div className="number">{postForm.capacity}명</div>
        <div
          className="plus-btn"
          onClick={() =>
            dispatchPostFrom({ type: EPostFormActionType.plusCapacity })
          }
        >
          +
        </div>
      </div>
      <button className="request-btn" onClick={submitFunc}>
        그룹 생성
      </button>
    </div>
  );
}

export default Post;
