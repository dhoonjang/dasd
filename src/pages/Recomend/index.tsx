import React, { useState } from "react";
import "./Recommend.scss";
import RecommendView from "../../component/RecommendView";
import { recommendApi } from "../../api/apiDefaultRes";
import TestView from "../../component/TestView";

enum ERecommendType {
  test,
  random,
}

function Recommend() {
  const [recommendType, setRecommendType] = useState<ERecommendType | null>(
    null
  );
  const place = recommendApi.place;

  return (
    <div className="Recommend">
      <h1>여행지 추천</h1>
      <div className="btn-flex">
        <button
          className={`test-btn ${
            recommendType === ERecommendType.test && "select"
          }`}
          onClick={() => setRecommendType(ERecommendType.test)}
        >
          성향 추천
        </button>
        <button
          className={`recommend-btn ${
            recommendType === ERecommendType.random && "select"
          }`}
          onClick={() => setRecommendType(ERecommendType.random)}
        >
          랜덤 추천
        </button>
      </div>
      {recommendType === ERecommendType.test && (
        <TestView recommend={recommendApi} />
      )}
      {recommendType === ERecommendType.random && (
        <RecommendView place={place} />
      )}
      {recommendType === null && (
        <div className="message">
          추천 방식을
          <br />
          고르세요!
        </div>
      )}
    </div>
  );
}

export default Recommend;
