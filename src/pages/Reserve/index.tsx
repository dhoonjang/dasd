import React, { useMemo } from "react";
import "./Reserve.scss";
import { guestHouseApi } from "../../api/apiDefaultRes";
import GuestHousePreview from "../../component/GuestHousePreview";
import { useParams } from "react-router-dom";
import GuestHouseView from "../../component/GuestHouseView";

function Reserve() {
  const { guesthouse_id } = useParams();
  const guestHouseListView = guestHouseApi.list.map((g) => (
    <GuestHousePreview key={g.일련번호} guestHouse={g} />
  ));

  const selectGuestHouse = guestHouseApi.list.find(
    (g) => String(g.일련번호) === guesthouse_id
  );

  const guestHouseView = useMemo(() => {
    if (!selectGuestHouse) return <></>;
    return <GuestHouseView guestHouse={selectGuestHouse} />;
  }, [selectGuestHouse]);

  return (
    <div className="Reserve">
      <h1>게스트 하우스</h1>
      {selectGuestHouse ? guestHouseView : guestHouseListView}
    </div>
  );
}

export default Reserve;
