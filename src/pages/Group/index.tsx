import React, { useMemo } from "react";
import "./Group.scss";
import { groupApi } from "../../api/apiDefaultRes";
import GroupView from "../../component/GroupView";
import { useParams } from "react-router-dom";
import GroupPreview from "../../component/GroupPreview";

function Group() {
  const { group_id } = useParams();

  const groupListView = groupApi.list
    .filter((g, i) => i > 3)
    .map((g) => <GroupPreview key={g.id} group={g} isMy={true} />);

  const selectGroup = groupApi.list.find((g) => String(g.id) === group_id);

  const groupView = useMemo(() => {
    if (!selectGroup) return <></>;
    return (
      <GroupView
        group={selectGroup}
        member={groupApi.selectMember}
        isMy={true}
      />
    );
  }, [selectGroup]);

  return (
    <div className="Group">
      <h1>내 그룹</h1>
      {selectGroup ? groupView : groupListView}
    </div>
  );
}

export default Group;
