export enum EReserveFormActionType {
  setStartDate,
  setEndDate,
  setCardNum,
  setCVCNum,
  setExpireDate,
}
export interface IReserveFormAction {
  type: EReserveFormActionType;
  str: string;
}

export interface IReserveFormState {
  date: string[];
  cardNum: string;
  cvcNum: string;
  expireDate: string;
}

export const initialReserveForm: IReserveFormState = {
  date: ["", ""],
  cardNum: "",
  cvcNum: "",
  expireDate: "",
};

export function reserveFormReducer(
  state: IReserveFormState,
  action: IReserveFormAction
) {
  switch (action.type) {
    case EReserveFormActionType.setStartDate:
      let newDate = [...state.date];
      newDate[0] = action.str;
      return {
        ...state,
        date: newDate,
      };
    case EReserveFormActionType.setEndDate:
      let newDate2 = [...state.date];
      newDate2[1] = action.str;
      return {
        ...state,
        date: newDate2,
      };
    case EReserveFormActionType.setCardNum:
      return {
        ...state,
        cardNum: action.str,
      };
    case EReserveFormActionType.setCVCNum:
      return {
        ...state,
        cvcNum: action.str,
      };
    case EReserveFormActionType.setExpireDate:
      return {
        ...state,
        expireDate: action.str,
      };
    default:
      return state;
  }
}
