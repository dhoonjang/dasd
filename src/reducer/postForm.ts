export enum EPostFormActionType {
  setDestination,
  setName,
  setStartDate,
  setEndDate,
  plusCapacity,
  minusCapacity,
}

export interface IPostFormAction {
  type: EPostFormActionType;
  str?: string;
}

export interface IPostFormState {
  name: string;
  destination: string;
  startDate: string;
  endDate: string;
  capacity: number;
}

export const initialPostForm: IPostFormState = {
  name: "",
  destination: "",
  startDate: "",
  endDate: "",
  capacity: 1,
};

export function postFormReducer(
  state: IPostFormState,
  action: IPostFormAction
) {
  switch (action.type) {
    case EPostFormActionType.setName:
      return {
        ...state,
        name: action.str ? action.str : "",
      };
    case EPostFormActionType.setStartDate:
      return {
        ...state,
        startDate: action.str ? action.str : "",
      };
    case EPostFormActionType.setEndDate:
      return {
        ...state,
        endDate: action.str ? action.str : "",
      };
    case EPostFormActionType.setDestination:
      return {
        ...state,
        destination: action.str ? action.str : "",
      };
    case EPostFormActionType.plusCapacity:
      return {
        ...state,
        capacity: state.capacity + 1,
      };
    case EPostFormActionType.minusCapacity:
      if (state.capacity > 1) {
        return {
          ...state,
          capacity: state.capacity - 1,
        };
      }
      return state;
    default:
      return state;
  }
}
