export interface ITestFormAction {
  index: number;
  bool: boolean;
}

export interface ITestFormState {
  test: boolean[];
}

export const initialTestForm: ITestFormState = {
  test: [true, true, true, true, true, true, true, true],
};

export function testFormReducer(
  state: ITestFormState,
  action: ITestFormAction
) {
  let newTest = [...state.test];
  newTest[action.index] = action.bool;

  return {
    test: newTest,
  };
}
