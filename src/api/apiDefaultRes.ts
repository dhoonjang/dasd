import { placeDestination, placeGuestHouse } from "./destination";

export interface IPlace {
  일련번호: string;
  카테고리1: string;
  카테고리2: string;
  컨텐츠명: string;
  지번주소: string;
  신주소: string;
  상세주소: string;
  전화번호: string;
  홈페이지: string;
  이용시간: string;
  이용요금: string;
  입장료: string;
  사용료: string;
  휴무일: string;
  메뉴정보: string;
  주변관광지: string;
  부대행사: string;
  행사기간: string;
  숙박정보: string;
  이용가능시설: string;
  소개: string;
  세부정보: string;
  man_capacity?: number;
  woman_capacity?: number;
  date?: string[];
}

export interface IRecommend {
  user_type: string;
  introduce: string;
  place: IPlace[];
  testplace: IPlace[];
}

export interface IGroup {
  id: number;
  name: string;
  destination: string;
  date: [string, string];
  capacity: number;
  memberNum: number;
}

export interface IQuestion {
  question: string;
  trueAnswer: string;
  falseAnswer: string;
}

export interface IGuest {
  id: number;
  name: string;
  introduction: string;
  address: string;
  date: string[];
  woman_capacity: number;
  man_capacity: number;
  phone: string;
}

export interface IMember {
  id: number;
  name: string;
  isLeader: boolean;
  gender: number;
}

export interface ITestApi {
  list: IQuestion[];
}

export interface IGroupAPI {
  list: IGroup[];
  selectMember: IMember[];
}

export interface IGuestHouseAPI {
  list: IPlace[];
}
export const defaultDateArray = ["20200803", "20200804", "20200806"];
export const defaultCapacity = [8, 10];

export const recommendApi: IRecommend = {
  user_type: "내향형",
  introduce: "잘 알려진 여행지, 활동량이 적은 여행 선호",
  place: placeDestination.filter(
    (p) => p.카테고리2 === "커피/디저트" || p.카테고리2 === "명소"
  ),
  testplace: placeDestination.filter((p) => p.카테고리2 === "커피/디저트"),
};

export const guestHouseApi: IGuestHouseAPI = {
  list: placeGuestHouse,
};

export const testApi: ITestApi = {
  list: [
    {
      question: "오랜만의 쉬는 날",
      trueAnswer: "집에서 논다",
      falseAnswer: "친구랑 나가서 논다",
    },
    {
      question: "나는 낯선 사람과 술을 마셔야 한다면",
      trueAnswer: "맥주",
      falseAnswer: "칵테일",
    },
    {
      question: "나의 패션 스타일?",
      trueAnswer: "클래식",
      falseAnswer: "캐주얼",
    },
    {
      question: "나는 평생 한 곳에만 살 수 있다면?",
      trueAnswer: "휘황찬란한 궁전",
      falseAnswer: "수수하고 편안한 집",
    },
    {
      question: "나는 팀플을 할 때? ",
      trueAnswer: "리드하는 편(팀장되기)",
      falseAnswer: "따라가는 편(팀원되기)",
    },
    {
      question: "나는 여행지로",
      trueAnswer: "산이 좋다",
      falseAnswer: "바다가 좋다",
    },
    {
      question: "나는 주로",
      trueAnswer: "도시를 찍는다",
      falseAnswer: "자연을 찍는다",
    },
    {
      question: "나는 예술 작품을 볼 때",
      trueAnswer: "신중하게 의도를 분석한다",
      falseAnswer: "있는 그대로 즐긴다",
    },
  ],
};

export const groupApi: IGroupAPI = {
  list: [
    {
      id: 1111,
      name: "여행 ㄱ",
      destination: "경주",
      date: ["20200803", "20200806"],
      capacity: 7,
      memberNum: 5,
    },
    {
      id: 1616,
      name: "섬나라 여행",
      destination: "제주",
      date: ["20200803", "20200806"],
      capacity: 7,
      memberNum: 5,
    },
    {
      id: 1515,
      name: "같이 여행 갈 사람",
      destination: "부산",
      date: ["20200803", "20200806"],
      capacity: 7,
      memberNum: 5,
    },
    {
      id: 1250,
      name: "맛난 거 먹을 사람!",
      destination: "전주",
      date: ["20200803", "20200806"],
      capacity: 7,
      memberNum: 5,
    },
    {
      id: 1231,
      name: "해돋이 볼사람",
      destination: "동해",
      date: ["20200803", "20200806"],
      capacity: 7,
      memberNum: 5,
    },
    {
      id: 1124,
      name: "같이 여행 갈 사람",
      destination: "부산",
      date: ["20200803", "20200806"],
      capacity: 7,
      memberNum: 5,
    },
    {
      id: 1212,
      name: "맛난 거 먹을 사람!",
      destination: "전주",
      date: ["20200803", "20200806"],
      capacity: 7,
      memberNum: 5,
    },
    {
      id: 3434,
      name: "해돋이 볼사람",
      destination: "동해",
      date: ["20200803", "20200806"],
      capacity: 7,
      memberNum: 5,
    },
  ],
  selectMember: [
    {
      id: 1,
      name: "한지은",
      gender: 1,
      isLeader: true,
    },
    {
      id: 2,
      name: "장동훈",
      gender: 0,
      isLeader: false,
    },
    {
      id: 3,
      name: "임학범",
      gender: 0,
      isLeader: false,
    },
    {
      id: 4,
      name: "김현슬",
      gender: 0,
      isLeader: false,
    },
    {
      id: 5,
      name: "노태형",
      gender: 0,
      isLeader: false,
    },
  ],
};
